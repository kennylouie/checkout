#!/bin/bash

set -u

main()
{
	need_cmd bash-sdk
	need_cmd git
	need_cmd jq

	local _repo _br
	get_repo
	get_branch

	ensure cd "${SDK_STATE_DIR}" && ensure git clone --single-branch --branch "${_br}" "${_repo}"

	say Done!
}

get_repo()
{
	_repo="$(bash-sdk prompt input -a \
		--message "Remote repository URL" \
		--name "repo" | jq --raw-output ".repo")"

	check_empty "${_repo}"
}

get_branch()
{
	_br="$(bash-sdk prompt input -a \
		--message "branch" \
		--name "branch" \
		--default-value "master" | jq --raw-output ".branch")"

	check_empty "${_br}"
}

ensure()
{
	if ! "$@"; then err "command failed: $*"; fi
}

check_empty()
{
	[ -n "$1" ] || err "cannot be empty"
}

# utility function to indicate to user that program is needed
need_cmd()
{
	if ! check_cmd "$1"; then
		err "need $1 (command not found)"
	fi
}

# utility function to check if a program exectuable is callable
check_cmd()
{
	type "$1" >/dev/null 2>&1
}

# utility function to echo error and exit program
err()
{
	say "$@" >&2
	exit 1
}

# utility function to print with a tagline
say()
{
	bash-sdk print "$(printf "%b" "@checkout: $@\n")"
}

main "$@"
