# checkout op

This @cto.ai/op clones a git repo into the shared state directory shared between ops in a @cto.ai/op workflow.

This op can be run in the cli (using the @cto.ai tool https://cto.ai/docs/getting-started) or over slack (with the @cto.ai slackbot https://cto.ai/docs/slackapp)

# To use in a workflow:
+ Add to the ops.yml steps e.g:
```
steps:
	- ops run @kennylouie/checkout:[LATEST_VERSION] --repo [YOUR_REPO] --branch [YOUR_TEST_BRANCH]
	- ...

```
# Building from source and running

+ clone this repo
+ ensure @cto.ai/ops is installed
+ build the op
```
ops build .
```

+ publish the op to your team that is associated with the slackbot
```
ops publish .
```
