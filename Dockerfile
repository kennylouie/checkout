FROM registry.cto.ai/official_images/bash:latest

WORKDIR /ops

RUN apt update && apt install -y git jq

ADD checkout.sh .
